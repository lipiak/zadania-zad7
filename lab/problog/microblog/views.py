
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.contrib import messages

from django import forms

from django.shortcuts import render
from django.http import HttpResponseRedirect

from models import Wpis


class DodajForm(forms.Form):
    autor = forms.CharField(max_length=100)
    wpis = forms.CharField(max_length=500)

def dodaj(request):
    form = DodajForm() # An unbound form
    return render(request, 'microblog/dodaj.html', {
        'forma': form,})


def add(request):
    if request.method == 'POST': # If the form has been submitted...
        # ContactForm was defined in the the previous section
        form = DodajForm(request.POST) # A form bound to the POST data
        if form.is_valid():
            pass# All validation rules pass
            # Process the data in form.cleaned_data
            # ...
    return HttpResponseRedirect('~/blog/') # Redirect after POST
