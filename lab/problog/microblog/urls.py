from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from django.views.generic import ListView

from models import Wpis
import views


urlpatterns = patterns('',
        url(r'^$', ListView.as_view(queryset=Wpis.objects.all(), context_object_name='wpisy', template_name='microblog/stub.html'), name='lista_wpisow'),
        url(r'^dodaj/', "microblog.views.dodaj"),
        url(r'^add/', views.add, name='add')
)