
Laboratorium #7
===============

1. Korzystając z frameworka Django w projekcie ``mysite`` stworzyć aplikację ``microblog``
   do microblogowania o podobnej funkcjonalności jak w ostatniej pracy domowej.
   Jedynym odstępstem jest brak autoryzacji użytkownika dodającego wiadomość,
   a zamiast tego powinno być pole do podania nazwy użytkownika lub jego pseudonimu.
   Aplikacja powinna współdziałać z *nginx*-em i *uwsgi* na serwerze laboratoryjnym.

2. Stworzyć testy jednostkowe dla każdej funkcjonalności aplikacji w pliku ``tests.py``