# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils import timezone

from django import forms

from django.shortcuts import render
from django.http import HttpResponseRedirect

from models import Wpis
from models import User


class DodajForm(forms.Form):
    wpis = forms.CharField(max_length=500)

class LoginForm(forms.Form):
    login = forms.CharField(max_length=100)
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)

class RegisterForm(forms.Form):
    login = forms.CharField(max_length=100)
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)
    nhaslo = forms.CharField(max_length=32, widget=forms.PasswordInput)

class FilterForm(forms.Form):
    by_username = forms.CharField(max_length=100)

def dodaj(request):
    form = DodajForm() # An unbound form
    return render(request, 'microblog/dodawanie.html', {
        'forma': form,
        'zalogowany': request.session['zalogowany']})

def login(request):
    forma = LoginForm()
    return render(request, 'microblog/login.html', {
        'forma': forma,
        'zalogowany': request.session['zalogowany']})

def register(request):
    forma = RegisterForm()
    return render(request, 'microblog/register.html', {
        'forma': forma,
        'zalogowany': request.session['zalogowany']})

def doLogin(request):
    forma = LoginForm(request.POST)

    if request.method == 'POST':

        if forma.is_valid():
            username = forma.cleaned_data['login']
            haslo = forma.cleaned_data['haslo']
            ilosc = User.objects.filter(login=username, haslo=haslo).count()
            if ilosc is 1:
                request.session['zalogowany'] = 'True'
                request.session['user'] = username
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'login_ok',)))
            else:
                request.session['zalogowany'] = 'False'
                request.session['user'] = None
                return HttpResponseRedirect(reverse('microblog.views.komunikat'))

    else:
        if request.session['zalogowany'] == 'True':
            return HttpResponseRedirect(reverse('microblog.views.komunikat'))
        else:
            return HttpResponseRedirect(reverse('microblog.views.login'))

def doRegister(request):
    forma = RegisterForm(request.POST)

    if request.method == 'POST':

        if forma.is_valid():
            username = forma.cleaned_data['login']
            haslo = forma.cleaned_data['haslo']
            nhaslo = forma.cleaned_data['nhaslo']
            ilosc = User.objects.filter(login=username).count()
            if ilosc >= 1:
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'user_exists',)))
            else:
                if not haslo == nhaslo:
                    return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'pwd_not_match',)))


                u = User(login=username, haslo=haslo)
                u.save()
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'register_ok',)))

    else:
        return HttpResponseRedirect(reverse('microblog.views.register'))

def komunikat(request, typ, message):
    tresc = ''
    if message == 'login_ok':
        tresc = 'Zalogowano pomyslnie'
    elif message == 'logout_ok':
        tresc = 'Wylogowano pomyslnie'
    elif message == 'not_logged_in':
        tresc = 'Nie jestes zalogowany'
    elif message == 'wpis_ok':
        tresc = 'Twoj wpis dodano pomyslnie'
    elif message == 'user_exists':
        tresc = 'Taki uzytkownik juz istnieje'
    elif message == 'pwd_not_match':
        tresc = 'Podane hasla nie sa identyczne'
    elif message == 'register_ok':
        tresc = 'Rejestracja poprawna. Mozesz sie zalogowac'
    else:
        tresc = 'Cos chyba kombinujesz'

    if typ == 'ok' or typ == 'blad':
        return render(request, 'microblog/komunikat.html',
            {
                'typ': typ,
                'tresc': tresc,
                'zalogowany': request.session['zalogowany'],
            })
    else:
        return render(request, 'microblog/komunikat.html',
            {
                'typ': 'blad',
                'tresc': tresc,
                'zalogowany': request.session['zalogowany'],
            })



def index(request):
    if not request.session.get('zalogowany'):
        request.session['zalogowany'] = 'False'

    return render(request, 'microblog/index.html', {
        'zalogowany': request.session['zalogowany'],
        'wszystkie_wpisy': Wpis.objects.all().order_by('-data'),
    })

def logout(request):
    if request.session['zalogowany'] == 'True':
        request.session['zalogowany'] = 'False'
        request.session['username'] = None
        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'logout_ok',)))
    else:
        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'not_logged_in',)))



def add(request):
    if request.session['zalogowany'] == 'True':
        if request.method == 'POST': # If the form has been submitted...
            # ContactForm was defined in the the previous section
            form = DodajForm(request.POST) # A form bound to the POST data
            if form.is_valid():
                user = request.session['user']
                tresc = form.cleaned_data['wpis']
                data = timezone.now()
                w = Wpis(autor=user, tresc=tresc, data=data)
                w.save()
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'wpis_ok',)))

        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'fdsfds',)))

    else:
        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'not_logged_in',)))

def filter(request):
    forma = FilterForm()
    return render(request, 'microblog/filter.html', {
        'forma': forma,
        'zalogowany': request.session['zalogowany']})

def doFilter(request):

    if request.method == 'POST':
        forma = FilterForm(request.POST)

        if forma.is_valid():
            user = forma.cleaned_data['by_username']
            return HttpResponseRedirect(reverse('microblog.views.showUserPosts', args=(user,)))

    return HttpResponseRedirect(reverse('microblog.views.index'))

def showUserPosts(request, user):
    if not request.session.get('zalogowany'):
        request.session['zalogowany'] = 'False'

    return render(request, 'microblog/index.html', {
        'zalogowany': request.session['zalogowany'],
        'wszystkie_wpisy': Wpis.objects.filter(autor=user).order_by('-data'),
        })


